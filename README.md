#volume-sftp-backup
This small container backups a volume to an SFTP Server.

####Configuration
Please provide a file "***sftp-config***" inside the volume ***/configdir*** (mount it)
```
SFTP_HOST="example.com" 
SFTP_PORT="21"
SFTP_USER="user"
SFTP_PASSWORD="mypassword"
```


###Build
```
docker build -t volume-sftp-backup .
```
###Run
Edit the sample configuration file or mount any other path containing a file using the following command:
```
docker run -v $PWD/sample:/configdir volume-sftp-backup -v /path/to/backup:/data volume-sftp-backup volume-sftp-backup
 
```
###Environment
```
# This will be the Prefix of the backup files
ENV ASSET_NAME="vsb-backup"

# A path on the sftp site to put the backups to, if provided is must exist!
ENV TARGET_FOLDER="/"

# Pass a date in the form yyyy-MM-dd to restore the backup from that day
ENV RESTORE=""
```
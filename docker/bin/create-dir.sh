#!/usr/bin/env bash
echo "Connection to $SFTP_HOST"
lftp <<SCRIPT
open $SFTP_HOST:$SFTP_PORT
user $SFTP_USER $SFTP_PASSWORD
mkdir -p $TARGET_FOLDER
exit
SCRIPT

echo "Backup complete"
rm -f $FILE
#!/bin/sh
source /configdir/sftp-config
FILE="$ASSET_NAME"-`date +"%Y-%m-%d"`.tar.gz
echo Creating $FILE
tar -zcvf "$FILE" /data -C /data


echo "Connection to $SFTP_HOST"
lftp <<SCRIPT
open $SFTP_HOST:$SFTP_PORT
user $SFTP_USER $SFTP_PASSWORD
#lcd /tmp
mkdir -p $TARGET_FOLDER
cd $TARGET_FOLDER
put $FILE
exit
SCRIPT

echo "Backup complete"
rm -f $FILE
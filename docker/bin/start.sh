#!/bin/sh

source /configdir/sftp-config

if [[ -z "$RESTORE" ]]
then
    backup.sh
else
    restore.sh
fi
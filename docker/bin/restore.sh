#!/bin/sh
source /configdir/sftp-config
FILE=$ASSET_NAME-$RESTORE.tar.gz
echo Restoring $FILE
echo "Connection to $SFTP_HOST"
lftp <<SCRIPT
open $SFTP_HOST:$SFTP_PORT
user $SFTP_USER $SFTP_PASSWORD
#lcd /data
cd $TARGET_FOLDER
get $FILE
exit
SCRIPT


tar -xzf "$FILE"

echo "Restore complete"

rm -f $FILE
FROM alpine:3.9

RUN apk add --update lftp && rm -rf /var/cache/apk/*

# This will be the Prefix of the backup files
ENV ASSET_NAME="vsb-backup"

# A path on the sftp site to put the backups to, if provided is must exist!
ENV TARGET_FOLDER="/"

# Pass a date in the form yyyy-MM-dd to restore the backup from that day
ENV RESTORE=""

COPY docker/bin /usr/local/bin
COPY ./data/sftp/conf/lftp.conf /etc/lftp.conf

VOLUME configdir
VOLUME data


CMD "start.sh"